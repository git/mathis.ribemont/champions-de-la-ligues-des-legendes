# Champion de la ligue des légendes

Mathis Ribemont

# Ce qui fonctionne

On peut voir la liste des champions, avec une pagination, avec les images qui s'affiche grâce à un converter. En cliquant sur un champion, une commande nous redirige sur le détail du champion, ses compétences,... On peut l'éditer, en modifiant son nom ou sa description par exemple. On peut aussi modifier les descriptions des compétences, et on peut aussi en rajouter. L'ajout se fait grâce à une commande.<br>
<br>
L'application n'est pas finie, y'a des trucs moches. J'ai essayé de mettre les notions importantes que je devais comprendre (MVVM, Commande...). Une notion importante qui manque est par exemple le dictionnaire observable.